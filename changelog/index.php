<!doctype html>
<html>
<head>

	<title>Fur Affinity Exodus Helper</title>

	<meta name="viewport" content="width=device-width, initial-scale=1">

	<?php include("../includes/assets.php") ?>


<body>

	<div class="container">

		<?php include('../includes/header.php'); ?>

		<div class="row">

			<div class="col-sm-6">

				<h2>Changelog</h2>

				<div class="well">
					<strong>1 August 2015</strong>
					<ul>
						<li>Code cleanup (mainly to add project to GitHub)</li>
						<li>Removed some old unused code</li>
					</ul>
				</div>

				<div class="well">

					<strong>31 May 2015</strong>
					<ul>
						<li><span class="label label-danger"><i class="fa fa-bug"></i></span> Fixed a bug where a new watch on the Fur Affinty account used to access profiles caused Exodus Helper to break</li>
					</ul>
				</div>
				<div class="well">
					<strong>29 March 2015</strong>
					<ul>
						<li><span class="label label-success"><i class="fa fa-asterisk"></i></span> Sexier main page interface</li>
						<li><span class="label label-success"><i class="fa fa-asterisk"></i></span> Added the ability to submit a Username Matchup for users with different names across networks</li>
						<li><span class="label label-success"><i class="fa fa-asterisk"></i></span> Added Back to Top button</li>
						<li><span class="label label-success"><i class="fa fa-asterisk"></i></span> Added iconography </li>
						<li>Code cleanup</li>
					</ul>

				</div>

				<div class="well">
					<strong>26 March 2015</strong>
					<ul>
						<li><span class="label label-success"><i class="fa fa-asterisk"></i></span> Improved speed for users already searched by implementing caching across all networks.</li>
					</ul>

				</div>

				<div class="well">
					<strong>25 March 2015</strong>
					<ul>
						<li><span class="label label-danger"><i class="fa fa-bug"></i></span> Fixed private accounts not working. It actually works now, I promise!</li>
					</ul>

				</div>

				<div class="well">

					<strong>24 March 2015</strong><br />
					Update #1:<br />
					<ul>
						<li><span class="label label-success"><i class="fa fa-asterisk"></i></span> DeviantArt Support</li>
						<li><span class="label label-success"><i class="fa fa-asterisk"></i></span> Inkbunny Support</li>
						<li><span class="label label-success"><i class="fa fa-asterisk"></i></span> SoFurry Support</li>
						<li>Visual tweaks</li>
					</ul>

					Update #2:<br />
					<ul>
						<li><span class="label label-success"><i class="fa fa-asterisk"></i></span> Support for Private FA Accounts</li>
						<li>Some code reorganization</li>
					</ul>
					Update #3:<br />
					<ul>
						<li>Theme Updated</li>
					</ul>



				</div>

				<div class="well">

					<strong>23 March 2015</strong>
					<ul>
						<li>Added Google Analytics events tracking</li>
					</ul>

				</div>

				<div class="well">

					<strong>22 March 2015</strong>
					<ul>
						<li><span class="label label-success"><i class="fa fa-asterisk"></i></span> Created this changelog :D</li>
						<li><span class="label label-success"><i class="fa fa-asterisk"></i></span> Added footer</li>
						<li><span class="label label-danger"><i class="fa fa-bug"></i></span> Fixed bug that caused only the first 200 users in the watchlist were checked</li>
						<li>Moved disclaimer</li>
						<li>Organized some of the code</li>
					</ul>

				</div>

				<div class="well">

					<strong>20 March 2015</strong>
					<ul>
						<li>Created</li>
					</ul>

				</div>

			</div>

			<div class="col-sm-6">

				<h2>Known Bugs &amp; Planned Features</h2>
				<div class="well">
					<p>
						Bugs and Planned Features have been moved to <a href="http://github.com/therealgitcub/fa-exodus-helper/issues">our GitHub issues page</a>.
					</p>
				</div>

			</div>

		</div>

		<?php include('../includes/footer.php') ?>

	</div>


</body>
</html>
