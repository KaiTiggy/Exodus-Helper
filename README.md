# Fur Affinity Exodus Helper
Automatically search your Fur Affinity watchlist for users on DeviantArt, Weasyl,
SoFurry, and Inkbunny.  
[![Website](https://img.shields.io/website-online-offline-brightgreen-red/http/exodushelper.com.svg?maxAge=2592000)](http://exodushelper.com)
[![license](https://img.shields.io/github/license/therealgitcub/exodushelper.svg?maxAge=2592000)](https://github.com/TheRealGitCub/exodushelper/blob/master/LICENSE)

### [Visit Site](http://exodushelper.com) • [Follow on Twitter](http://twitter.com/exodushelper)

![Preview](http://kobitate.com/assets/img/exodus.png)

## Dependencies

All dependencies are included in this repository unless otherwise noted. Many of
these already exist on the server where I host Exodus Helper and they are,
therefore, not included in this repo.

- [Twitter Bootstrap](https://github.com/twbs/bootstrap) (not included)
- [jQuery](https://github.com/jquery/jquery) (not included)
- [Bootstrap Social Buttons](https://github.com/lipis/bootstrap-social) (not included)
- [Font Awesome](https://github.com/FortAwesome/Font-Awesome) (not included)
- [Bootswatch Themes](http://bootswatch.com)
- [PHP Simple HTML DOM Parser](http://simplehtmldom.sourceforge.net/)
- [jQuery Sticky](https://github.com/garand/sticky)
- [jQuery Easing](http://gsgd.co.uk/sandbox/jquery/easing/)

## Build Notes

Please note the following when creating your own build of Exodus Helper:

1. Check the above dependencies for the ones that are not included. Most of them
are listed in `includes/assets.php`.
2. Please remove the Google Analytics code from this file as well.
3. The site needs a folder to save its cache to. It is currently set to
`fa-exodus-cache`, which should be located in the root directory of Exodus Helper
