<?php
    $notices = array();

    $notices[] = array(
        "type" => "warning",
        "content" => "<strong>Note: </strong> ".
            "Due to security changes on FA's side, Exodus Helper currently ".
            "cannot retrieve watchlists for accounts hidden from the public. ".
            "If you wish to use Exodus Helper immediately, please make your FA ".
            "account public. I am working to fix this issue. "
    );

?>

<!doctype html>
<html>
<head>

	<title>Fur Affinity Exodus Helper</title>

	<?php include("includes/assets.php") ?>

	<script src="js/main.js"></script>


</head>
<body>

<?php include("includes/matchup-modal.php") ?>

<div class="container">



	<?php
        include('includes/header.php');
        include('includes/step-1.php');
        include('includes/step-2.php');
        include('includes/footer.php');
    ?>


</div>

</body>
</html>
