<?php

    session_start();

    $time = microtime();
    $time = explode(' ', $time);
    $time = $time[1] + $time[0];
    $start = $time;

    require_once('networks/Weasyl.class.php');
    require_once('networks/DeviantArt.class.php');
    require_once('networks/Inkbunny.class.php');
    require_once('networks/SoFurry.class.php');
    require_once('networks/FurryNetwork.class.php');

    $weasyl = new Weasyl();
    $deviantart = new DeviantArt();
    $inkbunny = new Inkbunny();
    $sofurry = new SoFurry();
    $furrynetwork = new FurryNetwork();

    $networks = explode(",", $_GET['networks']);

    function networkSelected($network)
    {
        return in_array($network, $networks);
    }

    function checkNetwork($network, $username)
    {
        $check_network = $network->userExists($username);

        if ($check_network != false && is_array($check_network)) {
            return array(
                "profile" => $network->profile($network->profileURL($check_network["matched_username"]), $check_network["icon"]),
                "method" => "matchup"
            );
        } elseif ($check_network != false) {
            return array(
                "profile" => $network->profile($network->profileURL($username), $check_network),
                "method" => "auto"
            );
        } else {
            return false;
        }
    }


    $watching_display_name = $_GET['displayName'];
    $watching_name = strtolower($watching_display_name);
    $watching_userID = strtolower($_GET['userID']);

    $found_profiles = array();

    $networks = explode(",", $_GET['networks']);

    foreach ($networks as $n) {
        if ($n != "") {
            $network = ${$n};

            $check_network = checkNetwork($network, $watching_name);


            if ($check_network != false) {
                $found_profiles[$network->display_name] = $check_network;
            }

            if (strcmp($watching_name, $watching_userID) != 0) {
                $check_network = checkNetwork($network, $watching_userID);

                if ($check_network != false) {
                    $found_profiles[$network->display_name] = $check_network;
                }
            }
        }
    }

    $first_icon = reset($found_profiles);

    $first_icon = $first_icon["profile"]["icon"];

    if (sizeof($found_profiles) > 0) {
        ?>
			<div class="well clearfix found-user-well">
				<img class="pull-left img-responsive" src="<?php echo $first_icon ?>" style="margin-right:15px;" />
				<strong><?php echo $watching_display_name ?></strong><br />
				<a class="profile-link btn btn-primary" target="_blank" href="http://furaffinity.net/user/<?php echo $watching_userID ?>" style="margin-bottom:5px;" >FA Profile &gt;</a><br />

				<?php

                    $matchup = false;

        foreach ($found_profiles as $network_name => $profile) {
            if ($profile['method']=="matchup") {
                $matchup = true;
            } ?>
							<a class="profile-link btn btn-primary" target="_blank" href="<?php echo $profile['profile']["url"] ?>">
								<?php echo $network_name ?> Profile &gt;
							</a>
						<?php
        }

        if ($matchup) {
            ?>
							<button class="btn btn-default matchup-marker"><i class="fa fa-exchange"></i></button>
						<?php
        } ?>

			</div>
		<?php
    } else {
        /*
            ?>

            <div class="row">
                <div class="col-sm-6 col-sm-offset-3">
                    <div class="well well-sm">
                        <?php echo $watching_display_name ?> not found
                    </div>
                </div>
            </div>

            <?php
        */
    }
?>
