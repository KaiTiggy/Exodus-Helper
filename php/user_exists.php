<?php

    require_once('networks/Weasyl.class.php');
    require_once('networks/DeviantArt.class.php');
    require_once('networks/Inkbunny.class.php');
    require_once('networks/SoFurry.class.php');
    require_once('networks/FurryNetwork.class.php');
    require_once('networks/FurAffinity.class.php');

    $weasyl = new Weasyl();
    $deviantart = new DeviantArt();
    $inkbunny = new Inkbunny();
    $sofurry = new SoFurry();
    $furrynetwork = new FurryNetwork();
    $furaffinity = new FurAffinity();

    $network = ${$_GET['network']};

    $user_exists = $network->userExists(strtolower($_GET['username']));

    if ($user_exists != false) {
        header("HTTP/1.0 200 UserFound");
    } else {
        header("HTTP/1.0 500 UserNotFound");
    }
