<?php

    require_once('networks/Weasyl.class.php');
    require_once('networks/DeviantArt.class.php');
    require_once('networks/Inkbunny.class.php');
    require_once('networks/SoFurry.class.php');

    $weasyl = new Weasyl();
    $deviantart = new DeviantArt();
    $inkbunny = new Inkbunny();
    $sofurry = new SoFurry();

    $names = $_GET['new_names'];

    $fa_name = $_GET['username'];

    foreach ($names as $n => $name) {
        if ($name != "") {
            $network = ${$n};
            $network->saveMatchup($fa_name, $name);
        }
    }
