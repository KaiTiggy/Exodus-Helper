<?php

    class Network
    {
        public $baseURL;
        public $display_name;

        public $keys;
        private $all_keys;

        private $directory;

        public function __construct($url, $name)
        {
            $this->baseURL = $url;
            $this->display_name = $name;

            $this->directory = dirname(__FILE__);
            $this->directory = str_replace("/networks", "", $this->directory);

            include('api_keys.php');

            if (isset($this->all_keys[$this->display_name])) {
                $this->keys = $this->all_keys[$this->display_name];
            }
        }

        public function curlRequest($url)
        {
            $ch = curl_init();
            curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
            curl_setopt($ch, CURLOPT_HEADER, false);
            curl_setopt($ch, CURLOPT_FOLLOWLOCATION, true);
            curl_setopt($ch, CURLOPT_URL, $url);
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
            $output = curl_exec($ch);

            return $output;
        }

        public function curlRequestHeaders($url, $data)
        {
            $ch = curl_init();
            curl_setopt($ch, CURLOPT_URL, $url);
            curl_setopt($ch, CURLOPT_HTTPHEADER, $data);
            curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "GET");
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
            $output = curl_exec($ch);

            return $output;
        }

        public function curlRequestPost($url, $data=null)
        {
            $ch = curl_init();
            curl_setopt($ch, CURLOPT_URL, $url);
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);

            if ($data != null) {
                $data_string = "";

                foreach ($data as $key => $value) {
                    $data_string .= urlencode($key).'='.urlencode($value).'&';
                    rtrim($data_string, '&');
                }

                curl_setopt($ch, CURLOPT_POST, count($data));
                curl_setopt($ch, CURLOPT_POSTFIELDS, $data_string);
            }

            $output = curl_exec($ch);

            $curl_status = curl_getinfo($ch, CURLINFO_HTTP_CODE);

            if ($curl_status == 429 || $curl_status == 403) {
                return "RATELIMIT";
            } else {
                return $output;
            }
        }

        public function profile($url, $icon)
        {
            $username = str_replace($this->profileURL(""), "", $url);

            return array(
                "url" => $url,
                "icon" => $icon,
                "username" => $username
            );
        }

        public function apiURL($path)
        {
            return $this->baseURL . $path;
        }

        public function userCachePath($username = null)
        {
            if ($username != null) {
                return "../fa-exodus-cache/" .$username. ".json";
            } else {
                return "../fa-exodus-cache/";
            }
        }

        public function getUserCache($username)
        {
            $user_cache = $this->userCachePath($username);

            if (file_exists($user_cache)) {
                $accounts = json_decode(file_get_contents($user_cache), true);
                if (isset($accounts[$this->display_name])) {
                    return $accounts[$this->display_name];
                } else {
                    return false;
                }
            } else {
                return false;
            }
        }

        public function saveUserCache($username, $data)
        {
            $user_cache = $this->userCachePath($username);

            if (file_exists($user_cache)) {
                $cache = json_decode(file_get_contents($user_cache), true);
            }

            $cache[$this->display_name] = $data;

            $cache_json = json_encode($cache);

            $save_cache = file_put_contents($user_cache, $cache_json);

            return $save_cache;
        }

        public function saveMatchup($username, $new_username)
        {
            $username = strtolower($username);
            $new_username = strtolower($new_username);

            $iconURL = $this->userExists($new_username);

            $user_info = array(
                "matched_username" => $new_username,
                "icon" => $iconURL
            );

            $matchup_file = $this->userCachePath()."__matchup_list.txt";
            $matchups = file_get_contents($matchup_file);
            $matchups = json_decode($matchups, true);
            $matchups[$username][$this->display_name] = $new_username;
            $matchups = json_encode($matchups);

            file_put_contents($matchup_file, $matchups);

            echo $this->saveUserCache($username, $user_info);
        }

        public function getAllMatchups()
        {
            $matchup_file = $this->userCachePath()."__matchup_list.txt";
            $matchups = file_get_contents($matchup_file);

            echo $matchups;
        }



        public function getCacheCount()
        {
            return (iterator_count(new DirectoryIterator($this->userCachePath())) - 1);
        }
    }
