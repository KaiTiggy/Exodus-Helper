<?php
    require_once('simple_html_dom.php');
    require_once('Network.class.php');

    class FurAffinity extends Network
    {
        private $fa_profile_html;

        public function __construct()
        {
            parent::__construct("http://www.furaffinity.net/", "Fur Affinity");
        }

        public function curlRequest($url)
        {
            $ch = curl_init();
            curl_setopt($ch, CURLOPT_URL, $url);
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
            curl_setopt($ch, CURLOPT_COOKIE, $this->keys);
            $output = curl_exec($ch);
            curl_close($ch);

            return $output;
        }

        public function getProfile($username)
        {
            $fa_profile = "http://www.furaffinity.net/user/" . $username;
            $profile = str_get_html($this->curlRequest($fa_profile));

            return $profile;
        }

        public function userExists($username)
        {
            $profile = $this->getProfile($username);

            $test_link = $profile->find("a[href*=gallery/". $username ."]", 0);

            if (!$test_link) {
                return false;
            } else {
                return true;
            }
        }

        public function getWatchlistURL($username)
        {
            return "http://www.furaffinity.net/watchlist/by/".$username."/1";
        }

        public function getMaintable()
        {
            return $this->fa_profile_html->find(".maintable", 0)->plaintext;
        }

        public function getWatchlist($username)
        {
            $profile = $this->getProfile($username);

            if (stripos($profile->plaintext, "please log in") !== false) {
                header("HTTP/1.0 403 PrivateUser");
                return false;
            }

            $list_link = $this->getWatchlistURL($username);

            if ($list_link == false) {
                header("HTTP/1.0 500 UserNotFound");
                return false;
            }

            $list = file_get_html($list_link);
            $list_array = array();

            $list_page = 1;

            $links = $list->find('tbody tr td a');

            while (sizeof($links) > 0) {
                foreach ($links as $row) {
                    $watching_display_name = str_replace(" ", "", $row->plaintext);
                    $watching_name = strtolower($watching_display_name);
                    $watching_userID = strtolower($row->attr['href']);

                    $watching_userID = explode("/", $watching_userID);
                    $watching_userID = $watching_userID[2];

                    $list_array[] = array(
                        "displayName" => $watching_display_name,
                        "userID" => $watching_userID
                    );
                }

                $list_link = str_replace("/" . $list_page, "/". ($list_page+1), $list_link);

                $list = file_get_html($list_link);
                $list_page++;

                $links = $list->find('tbody tr td a');
            }

            $list_json = json_encode($list_array);
            return $list_json;
        }

        public function profileURL($username)
        {
            return "http://www.furaffinity.net/user/" . $username;
        }

        public function getCache()
        {
            return false;
        }

        public function getJournalsList($username)
        {
            $html = file_get_html($this->baseURL . "journals/" . $username);
            $journals = $html->find("[id^=jid]");
            $ret = array();
            foreach ($journals as $j) {
                $link = $j->find(".cat a", 0);
                $ret[] = array(
                    "title" => $link->plaintext,
                    "url" => $link->href
                );
            }
            return json_encode($ret);
        }

        public function countDeletedComments($link)
        {
            $str = $this->curlRequest($this->baseURL . $link);
            $html = str_get_html($str);

            $content = $html->find(".maintable", 0);

            $hiddenComments = $html->find(".comment-deleted");
            $adminHide = 0;

            foreach ($hiddenComments as $c) {
                if (stripos($c->plaintext, "administration") !== false) {
                    $adminHide++;
                }
            }

            return $adminHide;
        }

        public function checkForBan()
        {
            $req = $this->curlRequest("http://furaffinity.net");
            if (stripos($req, "your ip address has been banned")) {
                return true;
            }
            return false;
        }
    }
