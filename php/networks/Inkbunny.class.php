<?php

    require_once('Network.class.php');

    class Inkbunny extends Network
    {
        public function __construct()
        {
            parent::__construct("https://inkbunny.net/", "Inkbunny");
        }

        public function userExists($username)
        {
            $user_cached = $this->getUserCache($username);

            if ($user_cached != false) {
                return $user_cached;
            } else {
                $username = strtolower($username);

                $url = $this->apiURL("api_username_autosuggest.php?username=") . $username;
                $output = $this->curlRequest($url);

                $output = json_decode($output, true);

                $output = $output["results"];

                $found_user = false;

                foreach ($output as $r) {
                    $result_name = strtolower($r["value"]);
                    $result_icon = "https://inkbunny.net/usericons/large/" . $r['icon'];

                    if (strcmp($username, $result_name) == 0) {
                        $found_user = $result_icon;
                        break;
                    }
                }

                if ($found_user != false) {
                    $this->saveUserCache($username, $result_icon);
                    return $result_icon;
                } else {
                    return false;
                }
            }
        }

        public function profileURL($username)
        {
            return "http://www.inkbunny.net/" . $username;
        }
    }
