<?php

    require_once('Network.class.php');

    class SoFurry extends Network
    {
        public function __construct()
        {
            parent::__construct("http://api2.sofurry.com/", "SoFurry");
        }

        public function userExists($username)
        {
            $user_cached = $this->getUserCache($username);

            if ($user_cached != false) {
                return $user_cached;
            } else {
                $url = $this->apiURL("std/getUserProfile?username=") . $username;
                $output = $this->curlRequest($url);

                $output = json_decode($output, true);

                if (isset($output["useralias"])) {
                    $this->saveUserCache($username, "https://www.sofurryfiles.com/std/avatar?user=" . $output["userID"]);
                    return "https://www.sofurryfiles.com/std/avatar?user=" . $output["userID"];
                } else {
                    return false;
                }
            }
        }

        public function profileURL($username)
        {
            return "http://". $username .".sofurry.com/";
        }
    }
