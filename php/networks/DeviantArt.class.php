<?php

    require_once('Network.class.php');

    class DeviantArt extends Network
    {
        private $client_id;
        private $client_secret;

        public $ratelimit_failures = 0;

        public function __construct()
        {
            parent::__construct("https://www.deviantart.com/", "DeviantArt");
            $this->client_id = $this->keys["id"];
            $this->client_secret = $this->keys["secret"];
        }

        public function userExists($username)
        {
            $user_cached = $this->getUserCache($username);

            if ($user_cached != false) {
                return $user_cached;
            } else {
                $this->startSession();
                sleep(1);
                $url = $this->apiURL("api/v1/oauth2/user/profile/") . $username . "?access_token=" . $_SESSION['deviantart_access_token'];

                $profile = $this->curlRequestPost($url);
                $profile = json_decode($profile, true);

                if (isset($profile["error_code"])) {
                    return false;
                } else {
                    if (isset($profile["user"])) {
                        $this->saveUserCache($username, $profile["user"]["usericon"]);
                        return $profile["user"]["usericon"];
                    } elseif ($profile == "RATELIMIT") {
                        if ($ratelimit_failures <= 5) {
                            sleep($ratelimit_failures);
                            $this->userExists($username);
                        }
                        $ratelimit_failures++;
                    }
                }
            }
        }

        public function getAccessToken()
        {
            $data = array(
                "grant_type" => "client_credentials",
                "client_id" => $this->client_id,
                "client_secret" => $this->client_secret
            );

            $da_credentials = $this->curlRequestPost($this->apiURL("oauth2/token"), $data);
            $da_credentials = json_decode($da_credentials, true);

            $access_token = $da_credentials["access_token"];

            return $access_token;
        }

        public function checkAccessToken($token)
        {
            $data = array(
                "access_token" => $token
            );

            $check_token = $this->curlRequestPost($this->apiURL("api/v1/oauth2/placebo"), $data);
            $check_token = json_decode($check_token, true);

            return ($check_token["status"] == "success");
        }

        public function profileURL($name)
        {
            return "http://" . $name . ".deviantart.com";
        }

        public function startSession()
        {
            if (!isset($_SESSION['deviantart_access_token'])) {
                $_SESSION['deviantart_access_token'] = $this->getAccessToken();
            } elseif (!$this->checkAccessToken($_SESSION['deviantart_access_token'])) {
                $_SESSION['deviantart_access_token'] = $this->getAccessToken();
            }
        }
    }
