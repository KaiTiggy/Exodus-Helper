<?php

    require_once('Network.class.php');

    class FurryNetwork extends Network
    {
        public function __construct()
        {
            parent::__construct("http://beta.furrynetwork.com/api/", "FurryNetwork");
        }

        public function userExists($username)
        {
            $user_cached = $this->getUserCache($username);


            if ($user_cached != false) {
                return $user_cached;
            } else {
                $url = $this->apiURL("character/") . $username;
                $output = $this->curlRequest($url);

                $output = json_decode($output, true);

                if (isset($output["name"])) {
                    $this->saveUserCache($username, $output["avatars"]["small"]);
                    return $output["avatars"]["small"];
                } else {
                    return false;
                }
            }
        }

        public function profileURL($username)
        {
            return "http://beta.furrynetwork.com/" . $username;
        }
    }
