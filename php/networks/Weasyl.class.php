<?php

    require_once('Network.class.php');

    class Weasyl extends Network
    {
        public function __construct()
        {
            parent::__construct("https://www.weasyl.com/api/", "Weasyl");
        }

        public function userExists($username)
        {
            $user_cached = $this->getUserCache($username);

            if ($user_cached != false) {
                return $user_cached;
            } else {
                $url = $this->apiURL("users/") . $username ."/view";
                $output = $this->curlRequestHeaders($url, array('X-Weasyl-API-Key: ' . $this->keys));

                $output = json_decode($output, true);

                if (isset($output['error'])) {
                    if ($output['error']['name'] == "RecordMissing") {
                        return false;
                    } else {
                        return null;
                    }
                } else {
                    $this->saveUserCache($username, $output['media']["avatar"][0]["url"]);
                    return $output['media']["avatar"][0]["url"];
                }
            }
        }

        public function profileURL($username)
        {
            return "http://www.weasyl.com/~" . $username;
        }
    }
