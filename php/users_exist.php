<?php

    require_once('networks/Weasyl.class.php');
    require_once('networks/DeviantArt.class.php');
    require_once('networks/Inkbunny.class.php');
    require_once('networks/SoFurry.class.php');
    require_once('networks/FurryNetwork.class.php');
    require_once('networks/FurAffinity.class.php');

    $weasyl = new Weasyl();
    $deviantart = new DeviantArt();
    $inkbunny = new Inkbunny();
    $sofurry = new SoFurry();
    $furrynetwork = new FurryNetwork();
    $furaffinity = new FurAffinity();

    $names = $_GET['names'];

    $results = array();

    foreach ($names as $n => $name) {
        if ($name != "") {
            $network = ${$n};

            $user_exists = $network->userExists($name);

            if ($user_exists != false) {
                $results[$n] = true;
            } else {
                $results[$n] = false;
            }
        } else {
            $results[$n] = null;
        }
    }

    echo json_encode($results);
