<meta name="viewport" content="width=device-width, initial-scale=1">

<link href="/assets/css/bootstrap.css" rel="stylesheet">
<link href="/assets/css/bootstrap.social.css" rel="stylesheet">
<link href="/assets/css/font-awesome.min.css" rel="stylesheet">
<link href="https://bootswatch.com/superhero/bootstrap.min.css" rel="stylesheet">

<link rel="stylesheet" href="//maxcdn.bootstrapcdn.com/font-awesome/4.3.0/css/font-awesome.min.css">

<script src="/assets/js/jquery.js"></script>
<script src="/assets/js/bootstrap.min.js"></script>
<script src="/js/jquery.easing.js"></script>

<link rel="stylesheet" href="/css/main.css" />

<script src="local.js"></script>

<script>
(function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

  ga('create', 'UA-60995956-1', 'auto');
  ga('send', 'pageview');

</script>
