<div class="row">
	<div class="col-sm-12">

		<h1 class="page-header">
			Fur Affinity Exodus Helper
			<a href="http://twitter.com/exodushelper" target="_blank" class="btn btn-social btn-twitter pull-right"><i class="fa fa-twitter"></i>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;@exodushelper</a>
			<a href="http://github.com/therealgitcub/fa-exodus-helper" target="_blank" class="btn btn-social btn-github pull-right" style="margin-right: 5px;"><i class="fa fa-github" style="border-right: none;"></i>&nbsp;</a>
		</h1>

		<?php

            if (isset($notices)) {
                foreach ($notices as $n) {
                    ?>
						<div class="alert alert-<?php echo $n['type'] ?>" style="display: block !important;">
							<?php echo $n['content'] ?>
						</div>
					<?php
                }
            }
        ?>

	</div>
</div>
