<div class="col-sm-3">

	<div class="well well-sm">

		<strong>About Username Matchups</strong><br />
		<i class="fa fa-exchange fa-2x pull-left"></i>Users marked with the icon shown here were submitted as Username Matchups. This means that the user the account belongs to has different usernames on other networks.

		<a class="btn btn-danger btn-sm btn-block" href="mailto:kobi@kobitate.com?subject=Mailicious Username Matchup">Report Malicious Matchup</a>
	</div>

	<div class="well well-sm">

		<strong>Did you know?</strong><br />
		By using FA Exodus Helper, you are helping make the search process faster for other users! FA Exodus users have helped find <span class="users-found-stat"></span> users so far.

	</div>

	<!--div class="well well-sm">

		You have selected DeviantArt. Please note that, due to restrictions with the DeviantArt API, there is a 1 second delay added when checking users.

	</div-->

</div>
