<div id="username-fail" class="alert alert-danger"></div>

<div class="well">

	<h4 class="text-center" style="margin-top:0; margin-bottom: 15px;">Watchlist Searcher</h4>

	<input type="text" class="form-control" placeholder="Your FA Username" id="client-username" /><br />
	<button id="find-users" class="btn btn-success pull-right">Search</button>

	<strong>Sites to search:</strong><br />

	<input type="hidden" id="networks-string" value="weasyl" />

	<input type="checkbox" class="network-checkbox" data-network="weasyl" checked /> Weasyl
	&nbsp;&nbsp;<input type="checkbox" class="network-checkbox" data-network="furrynetwork" /> FurryNetwork<br />
	<input type="checkbox" class="network-checkbox" data-network="deviantart" /> DeviantArt
	&nbsp;&nbsp;<input type="checkbox" class="network-checkbox" data-network="sofurry" /> SoFurry
	&nbsp;&nbsp;<input type="checkbox" class="network-checkbox" data-network="inkbunny" /> Inkbunny

</div>
