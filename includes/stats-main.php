<div class="well" id="cache-stats-main">

	<div class="row">

		<div class="col-sm-6 text-center vertical-align-column">
			<h2 class="users-found-stat">...</h2>
			<p>
				Users found so far
			</p>
		</div>
		<div class="col-sm-6 vertical-align-column">
			<strong>Did you know?</strong>
			<br />
			By using FA Exodus Helper, you are helping make the search process faster for other users!
		</div>

	</div>

</div>
