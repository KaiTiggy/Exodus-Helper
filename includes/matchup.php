<div class="alert alert-success" id="matchup-success"></div>

<div class="well">

	<h4 class="text-center" style="margin-top:0; margin-bottom: 15px;">Do you have multiple usernames?</h4>
	<a data-toggle="modal" href="#username-matchup-modal" class="btn btn-block btn-info"><i class="fa fa-user-plus"></i> Submit Username Matchup</a>

	<br />
	<p>
		If your username is different from your Fur Affinity username on one or more of the sites checked by Exodus Helper, you can submit a Matchup to help your watchers find you on other sites! Click the button above to get started.
	</p>


</div>
