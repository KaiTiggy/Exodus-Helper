<div id="step-2-header" class="searching">
	<h2 id="searching">
		<span id="searching-text">Searching <span id="client-watch-count"></span> users...</span>
		<button id="cancel-search" class="btn btn-danger pull-right btn-sm"><span class="glyphicon glyphicon-remove"></span> Cancel</button>
		<button id="search-again" class="btn btn-primary pull-right btn-sm hidden">Search Again</button>
	</h2>

	<div id="progress-sticky">

		<div id="progress-info">
			Checking <span id="progress-current-user"></span>...<br />
			<span id="progress-current-num"></span>/<span id="progress-max-num"></span><br />
		</div>
		<div class="progress">
			<div class="progress-bar" role="progressbar" style="width: 0%;" id="progress-bar"></div>
		</div>

	</div>
</div>
