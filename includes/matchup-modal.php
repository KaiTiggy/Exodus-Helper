<?php

    function inputAddons()
    {
        ?>
			<span class="input-group-addon">
				<i class="fa fa-user fa-fw"></i>
				<i class="fa fa-ellipsis-h fa-fw hidden"></i>
				<i class="fa fa-check fa-fw text-success hidden"></i>
				<i class="fa fa-times fa-fw text-danger hidden"></i>
				<i class="fa fa-exclamation fa-fw text-info hidden"></i>
			</span>

		<?php
    }

    function networkInput($network)
    {
        ?>

			<p>
				<?php echo $network ?><br />
				<div class="input-group network-input-group" id="matchup-network-<?php echo strtolower($network) ?>">
					<?php inputAddons() ?>
					<input type="textbox" class="form-control">
				</div>
			</p>

		<?php
    }
?>

<!-- Modal -->
<div class="modal fade" id="username-matchup-modal" tabindex="-1" role="dialog" aria-labelledby="usernameMatchupModal" aria-hidden="true">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
				<h4 class="modal-title">Submit Username Matchup</h4>
			</div>
			<div class="modal-body">

				<div id="matchup-step-1" class="matchup-step">
					<div class="alert alert-info" style="display:block;">
						Enter in your FA Username below and hit next to continue. Please use the username you login with or else your name may not be matched up!
					</div>
					<div class="row">
						<div class="col-sm-6 col-sm-offset-3">
							<h4 class="text-center">Your FA Username</h4>
							<div class="input-group network-input-group" id="matchup-network-furaffinity">
								<?php inputAddons() ?>
								<input type="textbox" class="form-control">
							</div>
						</div>
					</div>
				</div>

				<div id="matchup-step-2" class="matchup-step hidden">
					<div class="alert alert-info" style="display:block;">Enter in your usernames for the following sites. If you do not have an account on a site, simply leave it blank.</div>

					<h4 class="text-center">Your Other Usernames</h4>

					<div class="row">
						<div class="col-sm-6">
							<?php
                                networkInput("Weasyl");
                                networkInput("DeviantArt");
                                networkInput("FurryNetwork");
                            ?>
						</div>

						<div class="col-sm-6">
							<?php
                                networkInput("Inkbunny");
                                networkInput("SoFurry");
                            ?>
						</div>
					</div>

				</div>

				<div id="matchup-step-3" class="matchup-step hidden">

					<div class="alert alert-info" style="display:block;">Verify that these are correct and click Save to finish.</div>

					<div class="row vertical-align-outer">
						<div class="col-sm-3 text-right vertical-align-column" id="matchup-name-furaffinity">
							<h3>...</h3>
							<h4>Fur Affinity</h4>
						</div>
						<div class="col-sm-1 text-center vertical-align-column">
							<span class="glyphicon glyphicon-chevron-right"></span>
						</div>
						<div class="col-sm-4 vertical-align-column">
							<div id="matchup-name-weasyl">
								<h3>...</h3>
								<h4>Weasyl</h4>
							</div>
							<div id="matchup-name-deviantart">
								<h3>...</h3>
								<h4>DeviantArt</h4>
							</div>
							<div id="matchup-name-furrynetwork">
								<h3>...</h3>
								<h4>FurryNetwork</h4>
							</div>
						</div>
						<div class="col-sm-4 vertical-align-column">
							<div id="matchup-name-inkbunny">
								<h3>...</h3>
								<h4>Inkbunny</h4>
							</div>
							<div id="matchup-name-sofurry">
								<h3>...</h3>
								<h4>SoFurry</h4>
							</div>

						</div>
					</div>

				</div>

				<ul class="pager">
					<li class="previous disabled"><a href="#" id="matchup-prev" data-stepnum="1"><i class="fa fa-arrow-left"></i> Back</a></li>
					<li class="next"><a href="#" id="matchup-next" data-stepnum="1">Next <i class="fa fa-arrow-right"></i></a></li>
				</ul>

			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
				<button type="button" class="btn btn-success disabled" id="matchup-save">Save</button>
			</div>
		</div><!-- /.modal-content -->
	</div><!-- /.modal-dialog -->
</div><!-- /.modal -->
