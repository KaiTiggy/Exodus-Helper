<div class="well" id="about-well">

	<h4 class="text-center">About this Tool</h4>
	<p>
		This page uses the APIs from Weasyl, DeviantArt, SoFurry, and Inkbunny to find users in your watchlist across the different platforms. Matching usernames will automatically be found, and users with different names can submit a Username Matchup to the right of this box.
	</p>
	<p>
		Enter your FA Username in the textbox, select the network(s) you wish to search, and hit the button to continue.
	</p>


</div>
