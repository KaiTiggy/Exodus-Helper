<div class="row">
	<div class="col-sm-8 col-sm-offset-2">
	<script async src="//pagead2.googlesyndication.com/pagead/js/adsbygoogle.js"></script>
		<!-- Exodus Helper -->
		<ins class="adsbygoogle"
		 style="display:block"
		 data-ad-client="ca-pub-3532604260326316"
		 data-ad-slot="6052037874"
		 data-ad-format="auto"></ins>
		<script>
		(adsbygoogle = window.adsbygoogle || []).push({});
		</script>
	</div>
</div>

<div class="row">
	<div class="col-sm-12">
		<footer>

			&copy; 2015 <a href="http://kobitate.com">Kobi Tate</a> | <a href="/changelog">Changelog &amp; Bugs</a>
			<p>
			I am not associated with Weasyl, Fur Affinity, IMVU, DeviantArt, Inkbunny, or SoFurry. <br />Use at your own risk. (Although, it really shouldn't hurt anything...)
			</p>
		</footer>
	</div>
</div>

<div id="back-to-top" data-toggle="tooltip" data-placement="left" title="Back To Top">
	<i class="fa fa-angle-double-up"></i>
</div>
