/*global alert,ga*/

var watchlist;
var cancelProcess = false;

function checkCacheCount(){
	
	$.ajax({
		url: 'php/get_cachecount.php',
		success: function(data){
			$(".users-found-stat").html(data);
		}
	});
}

function checkUserExists(network, username, before, success, error){
	$.ajax({
		url: "php/user_exists.php",
		dataType: 'html',
		data: {
			network: network,
			username: username
		},
		beforeSend: before,
		success: success,
		statusCode: {
			500 : error
		}
			
	});
}

function checkBanned(before, success, error) {
	$.ajax({
		url: "php/check_banned.php",
		dataType: 'html',
		beforeSend: before,
		success: success,
		statusCode: {
			500 : error
		}
	});
}

function sendEvent(category, sendValue) {
	if (sendValue !== '') {
		ga('send', 'event', 'Exodus Helper', category, sendValue);
	}
	return true;
}

function checkUser(i) {
	var displayName = watchlist[i].displayName;
	var userID = watchlist[i].userID;
	
	var networks = $("#networks-string").val();
	
	if (!cancelProcess) {
	
		$.ajax({
			url: 'php/check_user.php',
			dataType: 'html',
			method: 'GET',
			data: {
				displayName: displayName,
				userID: userID,
				networks: networks
			},
			beforeSend: function(){
				$("#progress-current-num").text(i);
				$("#progress-current-user").text(displayName);
			},
			success: function(data) {
				var progress = ( (i+1) / watchlist.length ) * 100;
				$("#found-users-container").append(data);
				$("#progress-bar").css("width", progress + "%");
				
				var thisWell = $("#found-users-container .well:last-child");
				
				thisWell.find(".matchup-marker").tooltip({
					placement: "auto top",
					title: "Found via Username Matchup"
				});
				
				if ( (i+1) < watchlist.length){
					checkUser(i+1);
				}
				else if (progress === 100) {
					$("#searching-text").text("Search complete!");
					$("#progress-bar").addClass('progress-bar-success');
					$("#progress-current-num").text(watchlist.length);
					$("#cancel-search").addClass("hidden");
					$("#search-again").removeClass("hidden");
					$("#progress-sticky").addClass("hidden");
					$("#step-2-header").removeClass("searching");
					
					var found_users = $("#found-users-container .well").length;
					
					sendEvent("Search Complete", found_users + "/" + watchlist.length + " users found");
				}
			
			}
		});
	
	}
	
	else {
		$("#searching-text").text("Search canceled.");
		$("#progress-bar").addClass('progress-bar-danger');
		$("#cancel-search").addClass("hidden");
		$("#search-again").removeClass("hidden");
		$("#progress-sticky").addClass("hidden");
		$("#step-2-header").removeClass("searching");
		sendEvent("Search Cancelled", "Cancelled at " + i + "/" + watchlist.length);
	}
	
}

// function updateStickyWidth(){
// 	$("#progress-sticky.affix").css('width',$(".col-sm-6").width());
// }

function checkScroll(){
	var s = $(window).scrollTop();
	
	if (s > 830) {
		$("#back-to-top").css({opacity: 0.7});
	}
	else {
		$("#back-to-top").css({opacity: 0});
	}
	
}

// function textboxError(textbox, alert){
// 	if (alert === undefined) {
// 		alert = "Error!";
// 	}
// }

function addonLoading(addon) {
	var parent = addon.parent();
	
	parent.find('.fa-user').addClass('hidden');
	parent.find('.fa-ellipsis-h').removeClass('hidden');
	parent.find('.fa-check').addClass('hidden');
	parent.find('.fa-times').addClass('hidden');
	parent.find('.fa-exclamation').addClass('hidden');
}

function addonError(addon) {
	var parent = addon.parent();
	
	parent.find('.fa-user').addClass('hidden');
	parent.find('.fa-ellipsis-h').addClass('hidden');
	parent.find('.fa-check').addClass('hidden');
	parent.find('.fa-times').removeClass('hidden');
	parent.find('.fa-exclamation').addClass('hidden');
}

function addonSuccess(addon) {
	var parent = addon.parent();
	
	parent.find('.fa-user').addClass('hidden');
	parent.find('.fa-ellipsis-h').addClass('hidden');
	parent.find('.fa-check').removeClass('hidden');
	parent.find('.fa-times').addClass('hidden');
	parent.find('.fa-exclamation').addClass('hidden');
}

function addonReset(addon) {
	var parent = addon.parent();
	
	parent.find('.fa-user').removeClass('hidden');
	parent.find('.fa-ellipsis-h').addClass('hidden');
	parent.find('.fa-check').addClass('hidden');
	parent.find('.fa-times').addClass('hidden');
	parent.find('.fa-exclamation').addClass('hidden');
}

function addonRequired(addon) {
	var parent = addon.parent();
	
	parent.find('.fa-user').addClass('hidden');
	parent.find('.fa-ellipsis-h').addClass('hidden');
	parent.find('.fa-check').addClass('hidden');
	parent.find('.fa-times').addClass('hidden');
	parent.find('.fa-exclamation').removeClass('hidden');
}

$(document).ready(function(){
	
	checkBanned(function(){}, function(data){
		if (data === "TRUE") {
			alert("Exodus Helper's IP address has been banned by Fur Affinity. " +
				"More information should be available at @ExodusHelper on Twitter. "
			);
		}
	}, function(){});
	
	checkCacheCount();
	
	$(".network-checkbox[data-network=furrynetwork]").tooltip({
		title: "FN Support is in Beta",
		placement: "bottom"
	});
	
	$(".network-checkbox").change(function(){
		
		var networksString = "";
		
		$(".network-checkbox").each(function(){
			var checked = $(this).prop('checked');
			
			if (checked) {
				networksString = networksString + "," + $(this).data('network');
			}
			
		});
		
		$("#networks-string").val(networksString);
		
	});
	
	$("#find-users").click(function(){
		
		var username = $("#client-username").val();
		
		var networksString = $("#networks-string").val();
		
		if (username === "") {
			alert("Please enter a username");
		}
		else if(networksString === "") {
			alert("Please select at least one network to check");
		}
		else {
			$.ajax({
				url: 'php/get_watchlist.php',
				method: 'GET',
				context: this,
				data: {
					fa_name: username,
					networks: networksString
				},
				beforeSend: function(){
					$("#username-fail").text("").hide();
					$(this).html('<i class="fa fa-spinner fa-pulse"></i>Loading...').toggleClass("btn-success").toggleClass("btn-warning");
				},
				success: function(data){
					watchlist = JSON.parse(data);
					
					$("#client-watch-count,#progress-max-num").text(watchlist.length);
					
					$("#step-1").addClass('hidden');
					$("#step-2").removeClass('hidden');
					
					$(this).text("Search").toggleClass("btn-success").toggleClass("btn-warning");
					
					$("textarea").val(watchlist);
					
					sendEvent("Search Started", "Searching: " + networksString +" / " +watchlist.length + " users");
					
					checkUser(0);
					
				},
				error: function(){
					$(this).html('Search').toggleClass("btn-success").toggleClass("btn-warning");
				},
				statusCode: {
					500 : function(xhr){
						var errorType = xhr.statusText;
						var errorText;
						switch (errorType) {
							case "UserNotFound" :
								errorText = "User not found! Make sure you are using the username you login with.";
								sendEvent("User Not Found", "Username Attempted: " + username);
								$(this).text("Search").toggleClass("btn-success").toggleClass("btn-warning");
								break;
							default :
								errorText = "Sorry, an unknown error has occured. ";
								sendEvent("Unknown error", errorType);
						}
						jQuery("#username-fail").text(errorText).slideToggle();
					},
					403 : function (xhr){
						var errorType = xhr.statusText;
						var errorText;
						switch (errorType) {
							case "Forbidden" :
								errorText = "User has a protected account! See above for more information";
								sendEvent("User Protected", "Username Attempted: " + username);
								break;
							}
						jQuery("#username-fail").text(errorText).slideToggle();
					}
				}

			});
		}
		
	});
	
	$("#cancel-search").click(function(){
		$(this).text("Cancelling...");
		cancelProcess = true;
	});
	
	$("#search-again").click(function(){
		cancelProcess = false;
		
		$("#step-2").addClass('hidden');
		$("#step-1").removeClass('hidden');
		
		$("#found-users-container").empty();
		
		$("#client-username").val("");
		
		$("#searching-text").html('<span id="searching-text">Searching <span id="client-watch-count"></span> users...</span>');
		$("#progress-bar").removeClass('progress-bar-danger').removeClass('progress-bar-success');
		$("#cancel-search").removeClass("hidden").html('<span class="glyphicon glyphicon-remove"></span> Cancel');
		$("#search-again").addClass("hidden");
		$("#progress-sticky").removeClass("hidden");
		$("#step-2-header").addClass("searching");
		
		$("#progress-bar").css("width", "0%");
		
		sendEvent("Search Again", "Search Again");
		
	});
	
	$("#progress-sticky").affix({
		offset: {
			top: 160,
			bottom: 0
		}
	});
	
	
	// Name Matchup variables
	var nameTextboxFurAffinity = $("#matchup-network-furaffinity input");
	
	var nameTextboxWeasyl = $("#matchup-network-weasyl input");
	var nameTextboxDeviantArt = $("#matchup-network-deviantart input");
	var nameTextboxSoFurry = $("#matchup-network-sofurry input");
	var nameTextboxInkbunny = $("#matchup-network-inkbunny input");
	var nameTextboxFurryNetwork = $("#matchup-network-furrynetwork input");
	
	var nameShowFurAffinity = $("#matchup-name-furaffinity");
	var nameShowWeasyl = $("#matchup-name-weasyl");
	var nameShowDeviantArt = $("#matchup-name-deviantart");
	var nameShowSoFurry = $("#matchup-name-sofurry");
	var nameShowInkbunny = $("#matchup-name-inkbunny");
	var nameShowFurryNetwork = $("#matchup-name-furrynetwork");
	
	var nameFurAffinity = "";
	var nameWeasyl = "";
	var nameDeviantArt = "";
	var nameSoFurry = "";
	var nameInkbunny = "";
	var nameFurryNetwork = "";
	
	$("#matchup-next").click(function(){
		var step = $(this).data('stepnum');
		var toStep = step+1;
		
		// var error = false;
		// var errorText = "";
		
		if (toStep === 2) {
			
			var faAddon = $("#matchup-network-furaffinity .input-group-addon");
			
			nameFurAffinity = nameTextboxFurAffinity.val();
		
			if (nameFurAffinity === "") {
				addonRequired(faAddon);
				nameTextboxFurAffinity.addClass("required");
			}
			else {
				
				checkUserExists("furaffinity", nameFurAffinity,
					function() {
						addonLoading(faAddon);
					},
					function () {
						$("#matchup-prev").parent().toggleClass("disabled");
						nameShowFurAffinity.find('h3').text(nameFurAffinity);
						
						$("#matchup-step-" + step + ",#matchup-step-" + toStep).toggleClass('hidden');
						
						$("#matchup-next,#matchup-prev").data('stepnum', toStep);
						
						addonReset(faAddon);
						
					},
					function(){
						nameTextboxFurAffinity.addClass("error");
						addonError(faAddon);
					}
				);
				
			}
			
			
		}
		else if (toStep === 3) {
			
			nameWeasyl = nameTextboxWeasyl.val();
			nameDeviantArt = nameTextboxDeviantArt.val();
			nameSoFurry = nameTextboxSoFurry.val();
			nameInkbunny = nameTextboxInkbunny.val();
			nameFurryNetwork = nameTextboxFurryNetwork.val();
			
			$.ajax({
				url: 'php/users_exist.php',
				data : {
					names : {
						weasyl: nameWeasyl,
						deviantart: nameDeviantArt,
						sofurry: nameSoFurry,
						inkbunny: nameInkbunny,
						furrynetwork: nameFurryNetwork
					}
				},
				beforeSend: function(){
					
					$("#matchup-step-2 .network-input-group").each(function(){
						if ($(this).find('input').val() !== ""){
							addonLoading($(this).find(".input-group-addon"));
						}
					});
					
					nameShowWeasyl.addClass("hidden");
					nameShowDeviantArt.addClass("hidden");
					nameShowSoFurry.addClass("hidden");
					nameShowInkbunny.addClass("hidden");
					nameShowFurryNetwork.addClass("hidden");
				},
				success: function(data) {
					
					data = JSON.parse(data);
					
					var networksError = false;
					var successCount = 0;
					
					if (data.weasyl) {
						addonSuccess($("#matchup-network-weasyl .input-group-addon"));
						nameTextboxWeasyl.addClass("success");
						nameShowWeasyl.removeClass("hidden");
						successCount++;
					}
					else if (data.weasyl !== null){
						addonError($("#matchup-network-weasyl .input-group-addon"));
						nameTextboxWeasyl.addClass("error");
						networksError = true;
					}
					
					if (data.deviantart) {
						addonSuccess($("#matchup-network-deviantart .input-group-addon"));
						nameTextboxDeviantArt.addClass("success");
						nameShowDeviantArt.removeClass("hidden");
						successCount++;
					}
					else if (data.deviantart !== null){
						addonError($("#matchup-network-deviantart .input-group-addon"));
						nameTextboxDeviantArt.addClass("error");
						networksError = true;
					}
					
					if (data.sofurry) {
						addonSuccess($("#matchup-network-sofurry .input-group-addon"));
						nameTextboxSoFurry.addClass("success");
						nameShowSoFurry.removeClass("hidden");
						successCount++;
					}
					else if (data.sofurry !== null) {
						addonError($("#matchup-network-sofurry .input-group-addon"));
						nameTextboxSoFurry.addClass("error");
						networksError = true;
					}
					
					if (data.inkbunny) {
						addonSuccess($("#matchup-network-inkbunny .input-group-addon"));
						nameTextboxInkbunny.addClass("success");
						nameShowInkbunny.removeClass("hidden");
						successCount++;
					}
					else if (data.inkbunny !== null){
						addonError($("#matchup-network-inkbunny .input-group-addon"));
						nameTextboxInkbunny.addClass("error");
						networksError = true;
					}
					
					if (data.furrynetwork) {
						addonSuccess($("#matchup-network-furrynetwork .input-group-addon"));
						nameTextboxFurryNetwork.addClass("success");
						nameShowFurryNetwork.removeClass("hidden");
						successCount++;
					}
					else if (data.furrynetwork !== null){
						addonError($("#matchup-network-furrynetwork .input-group-addon"));
						nameTextboxFurryNetwork.addClass("error");
						networksError = true;
					}
					
					if (!networksError && successCount > 0) {
						nameShowWeasyl.find('h3').text(nameWeasyl);
						nameShowDeviantArt.find('h3').text(nameDeviantArt);
						nameShowSoFurry.find('h3').text(nameSoFurry);
						nameShowInkbunny.find('h3').text(nameInkbunny);
						nameShowFurryNetwork.find('h3').text(nameFurryNetwork);
						
						$("#matchup-save").toggleClass('disabled');
						
						$("#matchup-next,#matchup-prev").data('stepnum', toStep);
						
						$("#matchup-step-" + step + ",#matchup-step-" + toStep).toggleClass('hidden');
						
						$("#matchup-next").parent().toggleClass("disabled");
					}
					else if (successCount === 0) {
						alert("Please enter at least one username");
					}
				}
			});
		}
		
 	});
 	
 	$("#matchup-save").click(function(){
	 	
	 	var newnames = "";
	 	
	 	if (nameWeasyl !== "") {
		 	newnames = newnames + nameWeasyl + " ";
	 	}
	 	
	 	if (nameDeviantArt !== "") {
		 	newnames = newnames + nameDeviantArt + " ";
	 	}
	 	
	 	if (nameSoFurry !== "") {
		 	newnames = newnames + nameSoFurry + " ";
	 	}
	 	
	 	if (nameInkbunny !== "") {
		 	newnames = newnames + nameInkbunny + " ";
	 	}
	 	
	 	$.ajax({
			url: 'php/save_matchup.php',
			data : {
				username: nameFurAffinity,
				new_names : {
					weasyl: nameWeasyl,
					deviantart: nameDeviantArt,
					sofurry: nameSoFurry,
					inkbunny: nameInkbunny
				}
			},
			success : function(){
				$("#username-matchup-modal").modal("hide");
				$("#matchup-success").text("Username Matchup successfully saved!").slideToggle().delay(3000).slideToggle();
				sendEvent("Matchup Saved", "Matched " + nameFurAffinity + " to : " + newnames );
			}
		});
 	});
 	
 	$(".network-input-group input.form-control").focus(function(){
	 	var parent = $(this).closest(".network-input-group");
	 	var addon = parent.find('.input-group-addon');
	 	
	 	if ($(this).hasClass('error') || $(this).hasClass('success') || $(this).hasClass("required")) {
		 	addonReset(addon);
			$(this).removeClass('error').removeClass('success').removeClass("required");
	 	}
 	});
 	
 	$("#matchup-prev").click(function(){
	 	var step = $(this).data('stepnum');
		var toStep = step-1;
		
		$("#matchup-step-" + step + ",#matchup-step-" + toStep).toggleClass('hidden');
		
		if (toStep === 1) {
			$(this).parent().toggleClass("disabled");
		}
		else if (toStep === 2) {
			$("#matchup-next").parent().toggleClass("disabled");
			$("#matchup-save").toggleClass('disabled');
		}
		
		$(this).data('stepnum', toStep);
		$("#matchup-next").data('stepnum', toStep);
 	});
 	
 	$("#back-to-top").click(function(){
	 	sendEvent("Back to Top", "");
	 	$('html,body').animate({ scrollTop: 0 }, 1000, 'easeOutQuart');
        return false;
 	}).tooltip();
	
	$("#username-matchup-modal").on('hidden.bs.modal', function () {
		var step = $("#matchup-next").data('stepnum');
		
		sendEvent("Matchup Modal Close", "Closed on Step " + step);
		
		$(this).find("input").val("");
		
		$(this).find(".input-group-addon").each(function(){
			addonReset($(this));
		});
		if (step !== 1) {
			$("#matchup-step-" + step + ",#matchup-step-1").toggleClass('hidden');
			
			$(this).data('stepnum', 1);
			$("#matchup-next").data('stepnum', 1);
		}
		
		$("#matchup-next").parent().removeClass("disabled");
		$("#matchup-prev").parent().addClass("disabled");
		$("#matchup-save").addClass('disabled');
		
		
	}).on('show.bs.modal', function () {
		sendEvent("Matchup Modal Launch", "");
		
	});
});

$(window).scroll(function(){
	checkScroll();
});